var e = "#upload_error_msg",
    t = "#loading_section",
    s = /.txt$/,
    a = {
        uiSettings: {},
        scrape_keywords_arr: [],
        scrape_keywords_file_name: "",
        load: function() {
            a.addEvents(), a.setSettingsValues(), a.updateScrapeState()
        },
        addEvents: function() {
            $(document).on("click", ".close-popup", function(e) {
                self.close()
            }), $(document).on("click", ".export-data-btn", function(e) {
                e.preventDefault(), sendMessage({
                    command: "getSettings"
                }, function(e) {
                    var t, s, i, r, n;
                    for (a.uiSettings = e.uiSettings, t = [], s = a.uiSettings.scrape_keywords_arr, i = 0; i < s.length; i++)
                        for (r = s[i].result_items_arr || [], n = 0; n < r.length; n++) t.push(r[n]);
                    sendMessage({
                        command: "downloadCSV",
                        data: t
                    }, function(e) {})
                })
            }), $(document).on("click", ".clear-data-btn", function(e) {
                confirm("Are you sure you want to clear data!") && (e.preventDefault(), sendMessage({
                    command: "getSettings"
                }, function(e) {
                    a.uiSettings = e.uiSettings, a.uiSettings.current_processed_count = 0, a.uiSettings.scrape_keywords_arr = [], sendMessage({
                        command: "saveUISettings",
                        data: a.uiSettings
                    }, function(e) {
                        location.reload()
                    })
                }))
            }), $(document).on("change", "#keywords_file", function(e) {
                a.uploadFile(e)
            }), $(document).on("change", '[name="scrape_limit_type"]', function(e) {
                "Limit" == $(this).val() ? $("#scrape_limit_section").show() : $("#scrape_limit_section").hide()
            }), $(document).on("submit", "#start-search-form", function(i) {
                var r, n, o, d, c, l, u, g, p, m, _;
                if (i.preventDefault(), r = $(this), !1, $(e).hide(), $("#authResSection").hide(), "Start Process" == (n = $(this).find('[type="submit"]')).text()) {
                    if (o = "Multiple", d = $('[name="scrape_limit_type"]:checked').val(), c = $("#scrape_limit").val(), "Limit" == d && ("" == c || 0 == c || "0" == c)) return $(e).text("You need to enter a scrape limit"), $(e).show(), !1;
                    if (l = 5, u = 10, g = 1, p = 2, "Multiple" == o)
                        if ("" == $("#keywords_file").val()) {
                            if ("" == $("#keywords_input").val()) return $(e).text("You need to enter a keywords OR \n You need to select a txt file for keywords"), $(e).show(), !1;
                            for (a.scrape_keywords_arr = [], m = $("#keywords_input").val().split(","), _ = 0; _ < m.length; _++) m[_].trim() && a.scrape_keywords_arr.push({
                                keyword: m[_].trim(),
                                status: "Pending"
                            })
                        } else if (1 != s.test(String($("#keywords_file").val()).toLowerCase())) return $(e).text("You need to select only txt file (Ex : sample-keywords.txt)"), $(e).show(), !1;
                    if (1 == !(a.scrape_keywords_arr.length > 0)) return $(e).text("keywords not found!"), $(e).show(), !1;
                    sendMessage({
                        command: "getSettings"
                    }, function(e) {
                        var s, i, o, m, _;
                        if (a.uiSettings = e.uiSettings, s = a.uiSettings.userRegistered, i = a.uiSettings.total_scrapped_count, o = a.uiSettings.authUser, m = a.uiSettings.last_status_check_time || "", 1 != s && parseInt(i) >= parseInt(atob("MTAwMDAwMDAwMDA="))) return $("#authResSection").show(), $("#authResSection").html('<div class="alert alert-warning alert-dismissible text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Warning!</strong> <i class="fa fa-info-circle"></i> You are not registered or your account is not approved. </a>. <br> If you are not register. Click here to <a href="register.html"> register </a><br><br> Plese Contact With Our Service Member other wise Mail us To All <a style="color: #6a7ddb;" href="mailto:info.r2dclub@gmail.com" target="_blank"> info.r2dclub@gmail.com </a>For Activation Of Extention</div>'), !1;
                        _ = a.checkAuthStatus(o, m), parseInt(i) >= parseInt(atob("MTAwMDAwMDAwMDA=")) && 1 == _ ? ($(t).show(), r.find('[type="submit"]').attr("disabled", "disabled"), sendMessage({
                            command: "checkAuthStatus",
                            data: {}
                        }, function(e) {
                            if ($(t).hide(), r.find('[type="submit"]').removeAttr("disabled"), $("#authResSection").show(), e && e == atob("MQ==")) n.text("Stop & Download CSV"), n.removeClass("start-proccess-btn"), n.addClass("stop-proccess-btn"), $("#currentProcessedCount").text(0), sendMessage({
                                command: "getSettings"
                            }, function(e) {
                                a.uiSettings = e.uiSettings;
                                var t = a.scrape_keywords_arr[0].keyword;
                                a.scrape_keywords_arr[0].status = "Inprogress", a.uiSettings.scrape_keywords_file_name = a.scrape_keywords_file_name, a.uiSettings.scrape_limit = parseInt(c), a.uiSettings.keyword_time_delay_min = parseInt(l), a.uiSettings.keyword_time_delay_max = parseInt(u), a.uiSettings.data_extract_time_delay_min = parseInt(g), a.uiSettings.data_extract_time_delay_max = parseInt(p), a.uiSettings.scrape_limit_type = d, a.uiSettings.scrape_keywords_arr = [], a.uiSettings.scrape_keywords_arr = a.scrape_keywords_arr, a.uiSettings.scrape_type = "Multiple", a.uiSettings.last_item_index = 0, a.uiSettings.current_processed_count = 0, a.uiSettings.scrapped_items_arr = [], a.uiSettings.scrape_status = "Inprogress", sendMessage({
                                    command: "saveUISettings",
                                    data: a.uiSettings
                                }, function(e) {
                                    sendMessage({
                                        command: "startSearchProcess",
                                        data: {
                                            Keywords_input: t
                                        }
                                    }, function(e) {})
                                })
                            });
                            else {
                                if (e && atob("Mg==")) return $("#authResSection").html('<div class="alert alert-warning alert-dismissible text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Warning!</strong> <i class="fa fa-info-circle"></i> You are not registered or your account is not approved. </a>. <br> If you are not register. Click here to <a href="register.html"> register </a><br><br> Plese Contact With Our Service Member other wise Mail us To All <a style="color: #6a7ddb;" href="mailto:info.r2dclub@gmail.com" target="_blank"> info.r2dclub@gmail.com </a>For Activation Of Extention</div>'), !1;
                                $("#authResSection").html('<div class="alert alert-danger alert-dismissible text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> <i class="fa fa-times"></i> Something went wrong please try again.</div>')
                            }
                        })) : (n.text("Stop & Download CSV"), n.removeClass("start-proccess-btn"), n.addClass("stop-proccess-btn"), $(".clear-data-btn,.export-data-btn").show(), $("#currentProcessedCount").text(0), sendMessage({
                            command: "getSettings"
                        }, function(e) {
                            a.uiSettings = e.uiSettings;
                            var t = a.scrape_keywords_arr[0].keyword;
                            a.scrape_keywords_arr[0].status = "Inprogress", a.uiSettings.scrape_keywords_file_name = a.scrape_keywords_file_name, a.uiSettings.scrape_limit = parseInt(c), a.uiSettings.keyword_time_delay_min = parseInt(l), a.uiSettings.keyword_time_delay_max = parseInt(u), a.uiSettings.data_extract_time_delay_min = parseInt(g), a.uiSettings.data_extract_time_delay_max = parseInt(p), a.uiSettings.scrape_limit_type = d, a.uiSettings.scrape_keywords_arr = [], a.uiSettings.scrape_keywords_arr = a.scrape_keywords_arr, a.uiSettings.scrape_type = "Multiple", a.uiSettings.last_item_index = 0, a.uiSettings.current_processed_count = 0, a.uiSettings.scrapped_items_arr = [], a.uiSettings.scrape_status = "Inprogress", sendMessage({
                                command: "saveUISettings",
                                data: a.uiSettings
                            }, function(e) {
                                sendMessage({
                                    command: "startSearchProcess",
                                    data: {
                                        Keywords_input: t
                                    }
                                }, function(e) {})
                            })
                        }))
                    })
                } else n.text("Start Process"), n.removeClass("stop-proccess-btn"), n.addClass("start-proccess-btn"), sendMessage({
                    command: "stopAutoBotProcess",
                    reason: "manual_stop"
                }, function(e) {})
            }), $(document).on("click", ".download-keywords-name-btn", function(e) {
                var t, s;
                e.preventDefault(), t = "data:text/csv;charset=utf-8," + escape("keyword Name 1\nkeyword Name 2\nkeyword Name 3"), (s = document.createElement("a")).href = t, s.style = "visibility:hidden", s.download = "sample-keywords.txt", document.body.appendChild(s), s.click()
            })
        },
        browserSupportFileUpload: function() {
            var e = !1;
            return window.File && window.FileReader && window.FileList && window.Blob && (e = !0), e
        },
        uploadFile: function(e) {
            var t, s;
            a.browserSupportFileUpload() ? (null, t = e.target.files[0], (s = new FileReader).onload = function(e) {
                try {
                    var s = e.target.result;
                    s && void 0 !== s && null != s && null != s && (a.parseDataFromFile(s), a.scrape_keywords_file_name = t.name)
                } catch (i) {}
            }, s.readAsText(t, "ISO-8859-1"), s.onerror = function() {
                $("#error_msg").text("Unable to read " + t.name), $("#error_msg").show()
            }) : ($("#error_msg").text("The File APIs are not fully supported in this browser!"), $("#error_msg").show())
        },
        parseDataFromFile: function(e) {
            var t, s;
            for (a.scrape_keywords_arr = [], t = e.split("\n"), s = 0; s < t.length; s++) t[s].trim() && a.scrape_keywords_arr.push({
                keyword: t[s].trim(),
                status: "Pending"
            })
        },
        checkAuthStatus: function(e, t) {
            var s, i = !1;
            return "" == t && (t = (new Date).getTime()), s = daysBetween(t, (new Date).getTime()), 1 == (i = (s = parseInt(s)) > 10 || 1 != e) && setTimeout(function() {
                sendMessage({
                    command: "getSettings"
                }, function(e) {
                    a.uiSettings = e.uiSettings, a.uiSettings.last_status_check_time = (new Date).getTime(), sendMessage({
                        command: "saveUISettings",
                        data: a.uiSettings
                    }, function(e) {})
                })
            }, 1e3), i
        },
        setSettingsValues: function() {
            sendMessage({
                command: "getSettings"
            }, function(e) {
                var t, s;
                a.uiSettings = e.uiSettings, t = a.uiSettings.scrape_status, s = $("#start-search-form").find('[type="submit"]'), "Finished" == t ? (s.text("Start Process"), s.removeClass("stop-proccess-btn"), s.addClass("start-proccess-btn")) : "Inprogress" == t ? (s.text("Stop & Download CSV"), s.removeClass("start-proccess-btn"), s.addClass("stop-proccess-btn")) : (s.text("Start Process"), s.removeClass("stop-proccess-btn"), s.addClass("start-proccess-btn"))
            })
        },
        showScrapeItemsList: function() {
            sendMessage({
                command: "getSettings"
            }, function(e) {
                var t, s, i, r, n, o, d, c;
                for (a.uiSettings = e.uiSettings, t = a.uiSettings.scrape_keywords_arr, s = [], i = 0; i < t.length; i++)
                    for (r = t[i].result_items_arr || [], n = 0; n < r.length; n++) s.push(r[n]);
                for (o = s, d = "", c = 0; c < o.length; c++) d += '<tr><td><div style="width: 20px;">' + (c + 1) + ' </div> </td><td><div style="width: 100px;">' + o[c].business_name + ' </div> </td><td><div style="width: 150px;">' + ("" != o[c].phone_number ? '<a href="tel:' + o[c].phone_number + '"> <i class="fa fa-phone"></i> ' + o[c].phone_number + "</a>" : "") + ' </div> </td><td><div style="width: 250px;">' + ("" != o[c].email ? '<a href="mailto:' + o[c].email + '"> <i class="fa fa-envelope"></i> ' + o[c].email + "</a>" : "") + ' </div> </td><td><div style="width: 100px;">' + o[c].full_dddress + '</div> </td><td><div style="width: 100px;">' + (o[c].street_address ? o[c].street_address : "") + ' </div> </td><td><div style="width: 100px;">' + (o[c].city ? o[c].city : "") + ' </div> </td><td><div style="width: 100px;">' + (o[c].state ? o[c].state : "") + ' </div> </td><td><div style="width: 100px;">' + (o[c].zip ? o[c].zip : "") + ' </div> </td><td><div style="width: 100px;">' + o[c].reviews_count + ' </div> </td><td><div style="width: 100px;">' + o[c].rating_score + ' </div> </td><td><div style="width: 100px;">' + o[c].category + ' </div> </td><td><div style="width: 220px;">' + ("" != o[c].website ? '<a target="_blank" href="' + (null != o[c].website && "" != o[c].website && "#" != o[c].website ? o[c].website : "#") + '">' + (null != o[c].website && "" != o[c].website && "#" != o[c].website ? '<i class="fa fa-link"></i>' + o[c].website : "") + "</a>" : "") + " </div> </td></tr>";
                "Finished" == a.uiSettings.scrape_status && o.length > 0 ? $(".clear-data-btn,.export-data-btn").show() : (0 == o.length && (d += '<tr><td class="text-center text-danger" colspan="13"> No data found! </td></tr>'), $(".clear-data-btn,.export-data-btn").hide()), $("#tbl_items_list").html(d)
            })
        },
        updateScrapeState: function() {
            sendMessage({
                command: "getSettings"
            }, function(e) {
                var t, s, i, r;
                if (a.uiSettings = e.uiSettings, a.setSettingsValues(), a.showScrapeItemsList(), t = a.uiSettings.total_scrapped_count, s = a.uiSettings.current_processed_count, $("#currentProcessedCount").text(s), $("#totalScrappedItems").text(t), i = a.uiSettings.userRegistered, r = a.uiSettings.authUser, 1 != i && (t = a.uiSettings.total_scrapped_count, parseInt(t) >= parseInt(atob("MTAwMDAwMDAwMDA=")))) return $("#authResSection").show(), $("#authResSection").html('<div class="alert alert-warning alert-dismissible text-center"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Warning!</strong> <i class="fa fa-info-circle"></i> You are not registered or your account is not approved. </a>. <br> If you are not register. Click here to <a href="register.html"> register </a><br><br> Plese Contact With Our Service Member other wise Mail us To All <a style="color: #6a7ddb;" href="mailto:info.r2dclub@gmail.com" target="_blank"> info.r2dclub@gmail.com </a>For Activation Of Extention</div>'), !1;
                1 != i && 1 != r || $('[href="register.html"]').hide()
            })
        }
    };

function sendMessage(e, t) {
    null != t && (e.callback = "yes"), chrome.runtime.sendMessage(e, t)
}

function sendMessageActiveTab(e, t) {
    chrome.tabs.query({
        active: !0,
        currentWindow: !0
    }, function(s) {
        chrome.tabs.sendMessage(s[0].id, e, t)
    })
}

function handleMessage(e, t) {
    switch (e.command) {
        case "rec_getSettings":
            a.uiSettings = e.data.uiSettings;
            break;
        case "updateScrapeState":
            a.updateScrapeState()
    }
    void 0 !== e.data && void 0 !== e.data.callback && "yes" == e.data.callback && (callback(), callback = null)
}

function treatAsUTC(e) {
    var t = new Date(e);
    return t.setMinutes(t.getMinutes() - t.getTimezoneOffset()), t
}

function daysBetween(e, t) {
    return (treatAsUTC(t) - treatAsUTC(e)) / 864e5
}
$(document).ready(function() {
    a.load(), chrome.runtime.onMessage.addListener(handleMessage)
});